# General Description

This is the download page for Pigeon a Linux Comand Line Messager. This is not a  
messager built from the ground up but rather uses several different commands  
and code snippets crafted together to make a messanger.  

# Required Software
Linux (Should work for most version of Linux)  
Bash ()  
Mail ()  
 
# Downloading
### Linux
##### Using SSH
```
[user@computer Directory] $ git pull git@gitlab.com:Bow_Ties_Are_Cool/CS_2408_Foundations_of_Computer_Science_II.git
```
### Website Download (All OS)
##### Downloading From The Gitlab Page
Click the download button on the GitLab page (Its highlighted in a red box)  
![Download Button Location](images/download_button_highlighted.png)  
Find the File (Depends on the OS and Browser)  
Decompress the File (Depends on the OS and Programs Installed)  

#Running
I will explain how to run the build script.
### Linux
```
[user@computer Directory]$ cd Pigeon_Download/
[user@computer Pigeon_Download]$ source ./build
```
The build script will set up everything after which you can run the pigeon  
comand  